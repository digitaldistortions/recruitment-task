'use strict';

import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as path from 'path';
import * as fs from 'fs';
import * as jsyaml from 'js-yaml';
import * as mongoose from 'mongoose';
import swagger from './swagger';
import {AppConfig} from './app.config';
import mongooseConfig from './mongoose.config';

class Server {
    public app: express.Application;
    private router: express.Router = express.Router();
    private swaggerSpec = fs.readFileSync(path.join(__dirname, '../../swagger/swagger.yaml'), 'utf8');
    private swaggerDoc = jsyaml.safeLoad(this.swaggerSpec);
    private dashboardPath: string;
    private apiPath: string;

    constructor() {
        this.app = express();
        this.apiPath = Server.escapeRegExp(this.swaggerDoc.basePath + '/');
        this.config();
        this.routes();
        this.swagger();
        this.mongooseConfig();
    }

    public static bootstrap(): Server {
        return new Server();
    }

    private static escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    }

    private config(): void {
        this.app.set('views', path.join(__dirname, '../views'));
        this.app.set('view engine', 'ejs');
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(express.static('public'));
    }

    private routes(): void {
        this.router.use((req, res, next) => {
            console.log(`I sense a disturbance in the force on url ${req.url}...`); // DEBUG
            next();
        });

        this.app.use(this.router);
        this.homeRoute();
    }

    private homeRoute(): void {
        const regexPattern = `^(${this.apiPath}|${this.dashboardPath})`;
        const regex = new RegExp(regexPattern);

        this.router.get('*', (req, res, next) => {
            if (regex.test(req.url)) {
                next();
            } else {
                res.render('index', {
                    title: AppConfig.TITLE,
                    basePath: AppConfig.BASE_PATH
                });
            }
        });
    }

    private swagger(): void {
        swagger(this.app, this.swaggerDoc);
    }

    private mongooseConfig(): void {
        mongooseConfig(mongoose);
    }
}

const server = Server.bootstrap();
module.exports = server.app;
