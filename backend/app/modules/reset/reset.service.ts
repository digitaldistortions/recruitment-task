import {ItemDetails, ItemListItemDetails} from '../../../../frontend/src/app/api';
import ItemsService from '../items/items.service';
import Items from '../items/items.model';

export default class ResetService {
    public static getSampleDataItem(index: number, quantity: number): Items | ItemListItemDetails | ItemDetails {
        const nameLengthMin = 7;
        const nameLengthMax = 15;
        const nameLengthRandom = Math.floor(Math.random() * (nameLengthMax - nameLengthMin)) + nameLengthMin;
        const title = this.randomCharGenerator(nameLengthRandom);
        const slug = title.toLowerCase();

        return {
            slug: `${slug}`,
            title: `${title}`,
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt quam ligula, vitae varius ipsum
                viverra at. Aenean id accumsan sapien, quis faucibus urna. In id sodales quam. Duis lectus erat, dictum cursus rhoncus vel,
                 vehicula vel erat. Nunc tempor felis efficitur posuere scelerisque. Morbi sed ipsum urna.`,
            thumbUrl: '/assets/images/item-thumb.png',
            imageUrl: '/assets/images/item-image.jpg',
            quantity
        };
    }

    public static getSampleData(): Array<Items> {
        const itemsCount = 20;
        const itemsArray = [];
        const minQuantity = 1;
        const maxQuantity = 10;

        for (let i = 1; i <= itemsCount; i++) {
            const quantity = Math.floor(Math.random() * (maxQuantity - minQuantity)) + minQuantity;
            itemsArray.push(this.getSampleDataItem(i, quantity));
        }

        return itemsArray;
    }

    public static async resetStock(): Promise<void> {
        const data = this.getSampleData();
        await this.removeItems();

        data.forEach(item => {
            ItemsService.addItem(item);
        });
    }

    public static async removeItems(): Promise<any> {
        return ItemsService.removeItems();
    }

    public static randomCharGenerator(length: number): string {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}
