import ResetService from './reset.service';

export function resetStockHandler(req, res, next) {
    ResetService.resetStock()
        .then(() => {
            res.status(200).send('Zresetowano poprawnie!');
        })
        .catch(err => {
            console.error(err);
            res.status(500).send('Podczas operacji wystąpił bład');
        });
}
