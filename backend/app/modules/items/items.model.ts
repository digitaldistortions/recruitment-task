import {pre, prop, Typegoose} from 'typegoose';
import {ItemDetails, ItemListItemDetails} from '../../../../frontend/src/app/api';
import {DocumentQuery, Query} from 'mongoose';

export default class Items extends Typegoose {

    public static createItem(content: ItemListItemDetails & ItemDetails): Promise<any> {
        const item = new ItemsModel(content);
        return item.save();
    }

    public static getRecordsCount(): Query<number> {
        return ItemsModel.find().countDocuments();
    }

    public static getProductsList() {
        return ItemsModel.find().select('slug title description thumbUrl quantity');
    }

    public static getProductBySlug(slug: string) {
        return ItemsModel.findOne({slug}).select('title description imageUrl quantity');
    }

    public static removeItems() {
        return ItemsModel.deleteMany({});
    }

    public static async handleItemBuy(slug: string, quantity: number): Promise<any> {
        const item = await ItemsModel.findOne({slug});
        if (item.quantity < quantity) {
            throw Error('Invalid quantity!');
        } else {
            return item.updateOne({quantity: item.quantity - quantity});
        }
    }

    @prop() public slug: string;
    @prop() public title: string;
    @prop() public description: string;
    @prop() public thumbUrl: string;
    @prop() public imageUrl: string;
    @prop() public quantity: number;
    @prop() public modifiedAt?: Date;
}

export const ItemsModel = new Items().getModelForClass(Items);
