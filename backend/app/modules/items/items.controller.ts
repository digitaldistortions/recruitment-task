import ItemsService from './items.service';
import {ItemDetails, ItemsList} from '../../../../frontend/src/app/api';
import {errorHandler} from '../../../core/core.controller';

export function itemsListQuery(req, res, next) {
    ItemsService.getItems()
        .then((data: ItemsList) => {
            res.json(data);
        })
        .catch(err => errorHandler(res, err));
}

export function itemDetailsQuery(req, res, next) {
    ItemsService.getItemBySlug(req.swagger.params.slug.value)
        .then((data: ItemDetails) => {
            if (!data) {
                return res.staus(404).send();
            }
            res.json(data);
        })
        .catch(err => errorHandler(res, err));
}
