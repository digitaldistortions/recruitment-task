import {ItemDetails, ItemsList} from '../../../../frontend/src/app/api';
import Items from './items.model';

export default class ItemsService {
    public static async getItemBySlug(slug: string): Promise<ItemDetails> {
        return Items.getProductBySlug(slug);
    }

    public static async getItems(): Promise<ItemsList> {
        const items = await this.getItemsList();
        const count = await this.getItemsCount();

        const parsedItems: Array<ItemDetails> = items
            .map(item => {
                return {
                    slug: item.slug,
                    title: item.title,
                    description: item.description.slice(0, 30) + '...',
                    imageUrl: item.thumbUrl,
                    quantity: item.quantity
                };
            });

        return {
            totalCount: count,
            data: parsedItems
        };
    }

    public static async addItem(item: Items): Promise<any> {
        return Items.createItem(item);
    }

    public static async getItemsCount(): Promise<number> {
        return Items.getRecordsCount();
    }

    public static async removeItems(): Promise<any> {
        return Items.removeItems();
    }

    public static async getItemsList(): Promise<Array<Items>> {
        return Items.getProductsList();
    }
}
