import {CartProductsView} from '../../../../frontend/src/app/api';
import Items from '../items/items.model';

export default class CartService {
    public static async buyItems(items: CartProductsView): Promise<any> {
        const promises: Array<Promise<any>> = [];

        items.data.forEach(item => {
            promises.push(Items.handleItemBuy(item.slug, item.quantity));
        });

        return Promise.all(promises);
    }
}
