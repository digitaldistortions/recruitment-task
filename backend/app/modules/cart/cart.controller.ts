import CartService from './cart.service';

export function cartBuyHandler(req, res, next) {
    const requestBody = req.swagger.params.content.value;

    CartService.buyItems(requestBody)
        .then(() => {
            res.status(200).send();
        })
        .catch(() => {
            res.status(500).send();
        });
}
