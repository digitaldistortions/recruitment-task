"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const items_service_1 = require("../items/items.service");
class ResetService {
    static getSampleDataItem(index, quantity) {
        const nameLengthMin = 7;
        const nameLengthMax = 15;
        const nameLengthRandom = Math.floor(Math.random() * (nameLengthMax - nameLengthMin)) + nameLengthMin;
        const title = this.randomCharGenerator(nameLengthRandom);
        const slug = title.toLowerCase();
        return {
            slug: `${slug}`,
            title: `${title}`,
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt quam ligula, vitae varius ipsum
                viverra at. Aenean id accumsan sapien, quis faucibus urna. In id sodales quam. Duis lectus erat, dictum cursus rhoncus vel,
                 vehicula vel erat. Nunc tempor felis efficitur posuere scelerisque. Morbi sed ipsum urna.`,
            thumbUrl: '/assets/images/item-thumb.png',
            imageUrl: '/assets/images/item-image.jpg',
            quantity
        };
    }
    static getSampleData() {
        const itemsCount = 20;
        const itemsArray = [];
        const minQuantity = 1;
        const maxQuantity = 10;
        for (let i = 1; i <= itemsCount; i++) {
            const quantity = Math.floor(Math.random() * (maxQuantity - minQuantity)) + minQuantity;
            itemsArray.push(this.getSampleDataItem(i, quantity));
        }
        return itemsArray;
    }
    static resetStock() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = this.getSampleData();
            yield this.removeItems();
            data.forEach(item => {
                items_service_1.default.addItem(item);
            });
        });
    }
    static removeItems() {
        return __awaiter(this, void 0, void 0, function* () {
            return items_service_1.default.removeItems();
        });
    }
    static randomCharGenerator(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}
exports.default = ResetService;
