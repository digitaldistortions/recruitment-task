"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const reset_service_1 = require("./reset.service");
function resetStockHandler(req, res, next) {
    reset_service_1.default.resetStock()
        .then(() => {
        res.status(200).send('Zresetowano poprawnie!');
    })
        .catch(err => {
        console.error(err);
        res.status(500).send('Podczas operacji wystąpił bład');
    });
}
exports.resetStockHandler = resetStockHandler;
