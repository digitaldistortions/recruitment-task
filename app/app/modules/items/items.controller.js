"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const items_service_1 = require("./items.service");
const core_controller_1 = require("../../../core/core.controller");
function itemsListQuery(req, res, next) {
    items_service_1.default.getItems()
        .then((data) => {
        res.json(data);
    })
        .catch(err => core_controller_1.errorHandler(res, err));
}
exports.itemsListQuery = itemsListQuery;
function itemDetailsQuery(req, res, next) {
    items_service_1.default.getItemBySlug(req.swagger.params.slug.value)
        .then((data) => {
        if (!data) {
            return res.staus(404).send();
        }
        res.json(data);
    })
        .catch(err => core_controller_1.errorHandler(res, err));
}
exports.itemDetailsQuery = itemDetailsQuery;
