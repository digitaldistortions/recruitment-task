"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typegoose_1 = require("typegoose");
class Items extends typegoose_1.Typegoose {
    static createItem(content) {
        const item = new exports.ItemsModel(content);
        return item.save();
    }
    static getRecordsCount() {
        return exports.ItemsModel.find().countDocuments();
    }
    static getProductsList() {
        return exports.ItemsModel.find().select('slug title description thumbUrl quantity');
    }
    static getProductBySlug(slug) {
        return exports.ItemsModel.findOne({ slug }).select('title description imageUrl quantity');
    }
    static removeItems() {
        return exports.ItemsModel.deleteMany({});
    }
    static handleItemBuy(slug, quantity) {
        return __awaiter(this, void 0, void 0, function* () {
            const item = yield exports.ItemsModel.findOne({ slug });
            if (item.quantity < quantity) {
                throw Error('Invalid quantity!');
            }
            else {
                return item.updateOne({ quantity: item.quantity - quantity });
            }
        });
    }
}
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", String)
], Items.prototype, "slug", void 0);
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", String)
], Items.prototype, "title", void 0);
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", String)
], Items.prototype, "description", void 0);
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", String)
], Items.prototype, "thumbUrl", void 0);
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", String)
], Items.prototype, "imageUrl", void 0);
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", Number)
], Items.prototype, "quantity", void 0);
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", Date)
], Items.prototype, "modifiedAt", void 0);
exports.default = Items;
exports.ItemsModel = new Items().getModelForClass(Items);
