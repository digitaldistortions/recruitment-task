"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const items_model_1 = require("./items.model");
class ItemsService {
    static getItemBySlug(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return items_model_1.default.getProductBySlug(slug);
        });
    }
    static getItems() {
        return __awaiter(this, void 0, void 0, function* () {
            const items = yield this.getItemsList();
            const count = yield this.getItemsCount();
            const parsedItems = items
                .map(item => {
                return {
                    slug: item.slug,
                    title: item.title,
                    description: item.description.slice(0, 30) + '...',
                    imageUrl: item.thumbUrl,
                    quantity: item.quantity
                };
            });
            return {
                totalCount: count,
                data: parsedItems
            };
        });
    }
    static addItem(item) {
        return __awaiter(this, void 0, void 0, function* () {
            return items_model_1.default.createItem(item);
        });
    }
    static getItemsCount() {
        return __awaiter(this, void 0, void 0, function* () {
            return items_model_1.default.getRecordsCount();
        });
    }
    static removeItems() {
        return __awaiter(this, void 0, void 0, function* () {
            return items_model_1.default.removeItems();
        });
    }
    static getItemsList() {
        return __awaiter(this, void 0, void 0, function* () {
            return items_model_1.default.getProductsList();
        });
    }
}
exports.default = ItemsService;
