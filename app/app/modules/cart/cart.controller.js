"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cart_service_1 = require("./cart.service");
function cartBuyHandler(req, res, next) {
    const requestBody = req.swagger.params.content.value;
    cart_service_1.default.buyItems(requestBody)
        .then(() => {
        res.status(200).send();
    })
        .catch(() => {
        res.status(500).send();
    });
}
exports.cartBuyHandler = cartBuyHandler;
