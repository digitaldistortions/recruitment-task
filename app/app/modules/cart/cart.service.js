"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const items_model_1 = require("../items/items.model");
class CartService {
    static buyItems(items) {
        return __awaiter(this, void 0, void 0, function* () {
            const promises = [];
            items.data.forEach(item => {
                promises.push(items_model_1.default.handleItemBuy(item.slug, item.quantity));
            });
            return Promise.all(promises);
        });
    }
}
exports.default = CartService;
