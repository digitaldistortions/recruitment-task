"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const APP_CONFIG = require('../../config/config.json');
class AppConfig {
}
AppConfig.BASE_PATH = APP_CONFIG['basePath'] || '';
AppConfig.ENV = APP_CONFIG['env'] || 'development';
AppConfig.MONGO = APP_CONFIG['mongo'] || null;
AppConfig.PORT = APP_CONFIG['port'] || 8080;
AppConfig.TITLE = APP_CONFIG['title'] || '';
AppConfig.SWAGGER_CONTROLLERS = APP_CONFIG['swaggerControllers'] || '';
exports.AppConfig = AppConfig;
