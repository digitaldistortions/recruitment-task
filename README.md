# Divante

## Pierwszy start

- Należy utworzyć konfigurację aplikacji `config/config.json` na podstawie pliku `config/config.json.dist`
- Następnie uruchamiamy serwer mongo
- Uruchamiamy aplikację backendową poleceniem `npm run backend`
- Aplikacja domyslnie będzie dostępna pod adresem `http://localhost:8080/`, serwowany będzie zbudowany frontend
- Aby wypełnić bazę danych danymi należy wejsć pod adres `http://localhost:8080/api/reset-stock`. Polecenie również służy do przywracania domyslnych stanów magazynowych dla produktów 

## Co zostało dodatkowo wykoane

- Backend dostarcza produkty z bazy danych Mongo
- Dodany został mechanizm dodawania produktów, z losowymi nazwami, z losowym stanem magazynowym
- Produkty w bazie mają parametr ilosci (stan magazynowy)
- W szczegółach produktu widać ile jest danego produktu na stanie
- W przypadku braku na stanie na listingu widoczny będzie odpowiedni badne na produkcie
- Dodany został przycisk Buy items wywietlany w przypadku, gdy mamy jakie produkty w koszyku
- Do koszyka możemy dodać produkty tylko te, które są na stanie i w ilosci nie większej niż stan magazynowy
- Proces zakupowy (Buy items) wysyła request do serwera i ilosć produktów w bazie się zmienia zgodnie z zamówioną ilocią przez użytkownika
- W koszyku, po lewej stronie od nazwy wyswietlana jest bieżąca iloć wybranego produktu w koszyku
- Całosć komunikacji po stronie backendu z bazą danych oparta jest na obietnicach. Wczesniejsza architektura również była oparta na obietnicach, dzięki czemu podpięcie źródła w postaci bazy danych nie zmieniło dużo logiki w backendzie i nie było oparczone dużą iloscią pracy (od początku przyjęte zostało rozwiązanie skalowalne)
- W procesie zakupowym, po poprawnej jego realizacji użytkownik przekierowany jest to thank you page, a koszyk lokalnie jest czyszczony
- Ilosć sumaryczna produktów w koszyku (wartosć przy Cart) odpowiada sumie ilosci wszystkich wybranych produktów
- Wdrożono dodatkowe poprawki i usprawnienia w mechanice frontendu
- Mniejsze poprawki wizualne we frontendzie zostały dodane
- Dodana możliwość resetowania stanu magazynwego i produktów (na nowo generowane losowe nazwy, slugi i stany magazynowe) poprzez adres `http://localhost:8080/api/reset-stock`
- Poprawiłem też kilka pomniejszych rzeczy w konfiguracji serwera
- Refactor koszyka - już nie jest przechowywana tablica, a obiekt, dzięki czemu zmniejszona została zmniejszona złożonosć obliczeniowa przy wyszukiwaniu po slugu i niektóre mechanizmy można było uproscić
- Delikatny refactor pozostałych rzeczy, co mi się rzucały w oczy
- Zakutalizowana dokumentacja swagger
- Dodane paczki do obsługi mongo do package.json
- Oraz inne poprawki i usprawnienia, o których pewnie zapomniałem napisać :)

## Uruchamianie oraz budowanie aplikacji

- Aby odpalić serwer backendowy należy wpisać polecenie: `npm run backend`
- Aby odpalić serwer dev dla Angulara należy wpisać polecenie: `npm run start`
- Zbudowanie aplikacji Angular odbywa się za pomocą polecenia `npm run build`
- Zbudowanie apliakcji backend uruchamiamy poleceniem `npm run backend:build`

## Użyte technologie:

- Na backendzie NodeJS wraz z ExpressJS
- Frontend Angular 8
- Dokumentacja do API napisana w Swaggerze. 

## Wykorzystanie swaggera w przypadku backendu

- Dokumentacja została wykorzystana w przypadku frontu do wygenerowania klienta, a dla backendu za pomocą bibloteki swagger-tools obsłguje routing API. Szczerze mówiąc bardziej to prezentuje w formie proof of concept aniżeli rozwiązania produkcyjnego. Ale działa w tym przypadku bez zarzutu i wprowadza ład i porządek w aplikacji :) .

## Pobieranie danych we frontendzie

- Dane pobierane z API są pobierane za pomocą Resolve w Angularze, przez co użytkownik nie doświadcza „pustych ekranów“, a loading indicator występuje na zmianach w routerze.

## Mechanika koszyka

- Produkty w koszyku dodatkowo zapisywane są w localStorage przeglądarki, więc po odświeżeniu użytkownik dalej je widzi.

## Przyszłosciowo, TODO

- wdrożenie animacji dla itemków w koszuku - przy dodawaniu i usuwaniu
- interakcja wyszukiwarki - powinien wyjeżdżać input
- poprawki do RWD - np menu schowane w hamburgerze
- można by popracować nad obsługą błędów, zarówno po stronie frontendu do odpowiedniego wyswietlania lub przekierowania na widok 404 jak i po stronie backendu (opracować jaki error handler)
- proces zakupowy mógł by wyswietlać wiadomosc w "dymku" (modal)
- poprawa UX procesu dodawania produktu do koszyka

## Demo

http://divante.loho.pl/
