import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LayoutModule} from './layout/layout.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ApiModule} from './api';
import {apiConfiguration} from './api/api-configuration';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        LayoutModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ApiModule.forRoot(apiConfiguration)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
