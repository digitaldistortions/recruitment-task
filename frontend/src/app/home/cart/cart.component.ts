import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartService} from './cart.service';
import {Subscription} from 'rxjs';
import {ItemDetails, ItemsService} from '../../api';
import {Router} from '@angular/router';
import {take} from 'rxjs/operators';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {

    private cartItemsSub: Subscription;
    public cartItemsArray: Array<ItemDetails> = [];

    constructor(private cartService: CartService,
                private itemsService: ItemsService,
                private router: Router) {
    }

    ngOnInit() {
        this.cartService.getCartItemsFromLocalStorage();

        this.cartItemsSub = this.cartService.cartItem$
            .subscribe(items => {
                const itemsKeys = Object.keys(items);
                this.cartItemsArray = [];

                itemsKeys.forEach((key) => {
                    this.cartItemsArray.push(items[key]);
                });
            });
    }

    public buyItems() {
        this.itemsService.cartBuyHandler({data: this.cartItemsArray})
            .pipe(take(1))
            .subscribe(() => {
                this.goToThankYouPage();
                this.cartService.clearCart();
            }, err => console.error(err));
    }

    private goToThankYouPage() {
        this.router.navigate(['thank-you']);
    }

    public cartCount(): number {
        let count = 0;
        this.cartItemsArray.forEach(item => count += item.quantity);
        return count;
    }

    ngOnDestroy(): void {
        if (this.cartItemsSub) {
            this.cartItemsSub.unsubscribe();
        }
    }
}
