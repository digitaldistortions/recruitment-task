import {Component, Input} from '@angular/core';
import {CartService} from '../cart.service';
import {ItemDetails} from '../../../api';

@Component({
    selector: 'app-cart-item',
    templateUrl: './cart-item.component.html',
    styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent {

    @Input() public item: ItemDetails;

    constructor(private cartService: CartService) {
    }

    public removeItem() {
        this.cartService.removeItemFromCart(this.item.slug);
    }

}
