import {Injectable} from '@angular/core';
import {ItemDetails} from '../../api';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CartService {

    private cartItemsSubject: BehaviorSubject<{ [slug: string]: ItemDetails }> = new BehaviorSubject({});
    public cartItem$: Observable<{ [slug: string]: ItemDetails }> = this.cartItemsSubject.asObservable();

    constructor() {
    }

    public addItemToCart(slug: string, item: ItemDetails) {
        const cartItems = this.cartItemsSubject.getValue();
        if (!cartItems[slug]) {
            cartItems[slug] = {
                quantity: 1
            };
        } else {
            cartItems[slug].quantity += 1;
        }

        cartItems[slug].title = item.title;
        cartItems[slug].slug = slug;

        this.cartItemsSubject.next(cartItems);
        this.updateLocalStorage();
    }

    public removeItemFromCart(slug: string) {
        const cartItems = this.cartItemsSubject.getValue();
        delete cartItems[slug];
        this.cartItemsSubject.next(cartItems);
        this.updateLocalStorage();
    }

    public getCartItemsFromLocalStorage() {
        const cartItems = JSON.parse(localStorage.getItem('shopCartItems')) || {};
        this.cartItemsSubject.next(cartItems);
    }

    private updateLocalStorage() {
        localStorage.setItem('shopCartItems', JSON.stringify(this.cartItemsSubject.getValue()));
    }

    public isItemInCart(slug: string): Observable<boolean> {
        return this.cartItem$
            .pipe(
                map(items => {
                        return !!items[slug];
                    }
                )
            );
    }

    public itemCountInCart(slug: string): Observable<number> {
        return this.cartItem$
            .pipe(map(items => {
                if (items[slug]) {
                    return items[slug].quantity;
                }

                return null;
            }));
    }

    public clearCart() {
        this.cartItemsSubject.next({});
        this.updateLocalStorage();
    }
}
