import {Component, Input, OnInit} from '@angular/core';
import {ItemDetails} from '../../../api';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() public item: ItemDetails;

  constructor() { }

  ngOnInit() {
  }

}
