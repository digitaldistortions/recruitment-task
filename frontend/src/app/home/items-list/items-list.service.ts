import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ItemsList, ItemsService} from '../../api';

@Injectable({
    providedIn: 'root'
})
export class ItemsListService implements Resolve<ItemsList> {

    constructor(private itemsServcie: ItemsService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ItemsList> {
        return this.getItems();
    }

    private getItems(): Observable<ItemsList> {
        return this.itemsServcie.itemsListQuery();
    }
}
