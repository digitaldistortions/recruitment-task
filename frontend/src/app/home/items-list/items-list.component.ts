import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ItemsList} from '../../api';

@Component({
    selector: 'app-items-list',
    templateUrl: './items-list.component.html',
    styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit {
    public items: ItemsList;

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.items = this.route.snapshot.data.items.data;
    }

}
