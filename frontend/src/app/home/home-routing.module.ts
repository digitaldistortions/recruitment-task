import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ItemsListComponent} from './items-list/items-list.component';
import {ShopComponent} from './shop/shop.component';
import {ItemsListService} from './items-list/items-list.service';
import {ItemDetailsComponent} from './item-details/item-details.component';
import {ItemDetailsService} from './item-details/item-details.service';

const routes: Routes = [
    {
        path: '',
        component: ShopComponent,
        children: [
            {
                path: '',
                component: ItemsListComponent,
                resolve: {
                    items: ItemsListService
                }
            },
            {
                path: 'product/:slug',
                component: ItemDetailsComponent,
                resolve: {
                    item: ItemDetailsService
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule {
}
