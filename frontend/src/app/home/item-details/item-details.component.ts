import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ItemDetails} from '../../api';
import {CartService} from '../cart/cart.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-item-details',
    templateUrl: './item-details.component.html',
    styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {
    public item: ItemDetails;
    private itemSlug: string;

    constructor(private route: ActivatedRoute,
                private cartService: CartService) {
    }

    ngOnInit() {
        this.item = this.route.snapshot.data.item;
        this.itemSlug = this.route.snapshot.params.slug;
        this.isItemInCart();
    }

    public addToCart(): void {
        this.cartService.addItemToCart(this.itemSlug, this.item);
    }

    public isItemInCart(): Observable<boolean> {
        return this.cartService.isItemInCart(this.itemSlug);
    }

    public quantityFitToStock(): Observable<boolean> {
        return this.cartService.itemCountInCart(this.itemSlug)
            .pipe(map(count => {
                return count >= this.item.quantity;
            }));
    }

}
