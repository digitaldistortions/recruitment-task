import {Injectable} from '@angular/core';
import {ItemDetails, ItemsService} from '../../api';
import {Observable, of, throwError} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {catchError} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ItemDetailsService implements Resolve<ItemDetails> {

    constructor(private itemsService: ItemsService,
                private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ItemDetails> {
        return this.getItemDetails(route.params.slug);
    }

    private getItemDetails(slug: string): Observable<ItemDetails> {
        return this.itemsService.itemDetailsQuery(slug)
            .pipe(catchError(err => {
                this.router.navigate(['/']);
                return throwError(err);
            }));
    }
}
