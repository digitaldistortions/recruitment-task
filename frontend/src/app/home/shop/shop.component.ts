import {Component} from '@angular/core';
import {fadeAnimation} from '../../app.animations';

@Component({
    selector: 'app-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.scss'],
    animations: [fadeAnimation]
})
export class ShopComponent {
    constructor() {
    }


}
