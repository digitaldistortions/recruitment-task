import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {CartComponent} from './cart/cart.component';
import {ItemsListComponent} from './items-list/items-list.component';
import {ItemDetailsComponent} from './item-details/item-details.component';
import {ItemComponent} from './items-list/item/item.component';
import {ShopComponent} from './shop/shop.component';
import {SharedModule} from '../shared/shared.module';
import { CartItemComponent } from './cart/cart-item/cart-item.component';

@NgModule({
    declarations: [
        CartComponent,
        ItemsListComponent,
        ItemDetailsComponent,
        ItemComponent,
        ShopComponent,
        CartItemComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        HomeRoutingModule
    ]
})
export class HomeModule {
}
