import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {ContentComponent} from './content/content.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';

const COMPONENTS = [
    HeaderComponent,
    ContentComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS
    ],
    imports: [
        CommonModule,
        RouterModule,
        SharedModule
    ],
    exports: [
        ...COMPONENTS
    ]
})
export class LayoutModule {
}
