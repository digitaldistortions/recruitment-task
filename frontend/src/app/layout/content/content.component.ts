import {Component, OnDestroy, OnInit} from '@angular/core';
import {fadeAnimation} from '../../app.animations';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {NG_IF_ANIMATION} from '../../shared/animations/ng-if.animation';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss'],
    animations: [fadeAnimation, ...NG_IF_ANIMATION]
})
export class ContentComponent implements OnInit, OnDestroy {
    public loading: boolean;
    private routerEventsSub: Subscription;

    constructor(private router: Router) {
    }

    ngOnInit() {
        this.routerEventsSub = this.router.events
            .subscribe((event: any) => {
                if (event instanceof NavigationStart) {
                    this.loading = true;
                } else if (event instanceof NavigationEnd ||
                    event instanceof NavigationCancel ||
                    event instanceof NavigationError) {
                    this.loading = false;
                }
            });
    }

    ngOnDestroy(): void {
        if (this.routerEventsSub) {
            this.routerEventsSub.unsubscribe();
        }
    }
}
