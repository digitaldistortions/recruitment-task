import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonsModule} from './buttons/buttons.module';
import {LoaderModule} from './loader/loader.module';
import {BoxModule} from './box/box.module';

const MODULES = [
    ButtonsModule,
    LoaderModule,
    BoxModule
];

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ...MODULES
    ],
    exports: [
        ...MODULES
    ]
})
export class SharedModule {
}
