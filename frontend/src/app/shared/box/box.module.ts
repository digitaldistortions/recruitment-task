import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BoxComponent} from './box/box.component';
import {BoxIconComponent} from './box/box-icon/box-icon.component';
import {BoxContentComponent} from './box/box-content/box-content.component';

const COMPONENTS = [
    BoxComponent,
    BoxIconComponent,
    BoxContentComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ...COMPONENTS
    ]
})
export class BoxModule {
}
