import {Component, ViewChild} from '@angular/core';

@Component({
    selector: 'app-box-icon',
    templateUrl: './box-icon.component.html',
    styles: ['']
})
export class BoxIconComponent {

    @ViewChild('content', {static: true}) public content;

    constructor() {
    }

}
