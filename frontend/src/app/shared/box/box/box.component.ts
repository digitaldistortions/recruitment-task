import {Component, ContentChild, OnInit} from '@angular/core';
import {BoxIconComponent} from './box-icon/box-icon.component';
import {BoxContentComponent} from './box-content/box-content.component';

@Component({
    selector: 'app-box',
    templateUrl: './box.component.html',
    styleUrls: ['./box.component.scss']
})
export class BoxComponent {
    @ContentChild(BoxIconComponent, {static: true}) public boxIconContent: BoxIconComponent;
    @ContentChild(BoxContentComponent, {static: true}) public boxContent: BoxContentComponent;

    constructor() {
    }
}
