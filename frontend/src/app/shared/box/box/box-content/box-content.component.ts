import {Component, ViewChild} from '@angular/core';

@Component({
    selector: 'app-box-content',
    templateUrl: './box-content.component.html',
    styles: ['']
})
export class BoxContentComponent {

    @ViewChild('content', {static: true}) public content;

    constructor() {
    }

}
