import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from './button/button.component';

const COMPONENTS = [
    ButtonComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ...COMPONENTS
    ]
})
export class ButtonsModule {
}
